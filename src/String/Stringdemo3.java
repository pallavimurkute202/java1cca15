package String;

public class Stringdemo3 {
    public static void main(String[] args) {
        String s="Software Developer";
        System.out.println(s.length());
        System.out.println(s.charAt(12));
        System.out.println(s.indexOf("w"));
        System.out.println(s.lastIndexOf("e"));
        System.out.println(s.contains("eve"));
        System.out.println(s.startsWith("Soft"));
        System.out.println(s.endsWith("per"));
        System.out.println(s.substring(9));
        System.out.println(s.substring(0,8));
        System.out.println(s.toUpperCase());
        System.out.println(s.toLowerCase());
    }
}
