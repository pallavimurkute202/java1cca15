package String;

import java.util.Scanner;

public class StringDemo5 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Enter String:");
        String s=sc1.nextLine();
        String s1=s.toLowerCase();
        char[] data=s1.toCharArray();
        int vCount=0;
        int cCount=0;
        for (int a=0;a< data.length;a++){
            if (data[a]=='a' || data[a]=='e' || data[a]=='i' || data[a]=='o' || data[a]=='u'){
                vCount++;
            }
            else{
                cCount++;
            }
        }
        System.out.println("Totsl no of vowels:"+vCount);
        System.out.println("Total no of consonants:"+cCount);
    }
}
