package Abstract;
//sub class
public class Watchman extends Employee{
    @Override
    void getDesignation() {
        System.out.println("Designation is SR Watchman");
    }

    @Override
    void getSalary() {
        System.out.println("Salary is 400000");
    }
}
