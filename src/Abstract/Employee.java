package Abstract;
//super class
public abstract class Employee {
    abstract void getDesignation();
    abstract void getSalary();
}
