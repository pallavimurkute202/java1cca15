package Abstract;
//sub class
public class Manager extends Employee{
    @Override
    void getDesignation() {
        System.out.println("Designation is manager");
    }

    @Override
    void getSalary() {
        System.out.println("Salary is 1500000");
    }
}
