package Abstract;

public abstract class Demo {
    //static & non-static variables
    static int k=20;
    double b=23.45;
    //non-static abstract & concrete method
    abstract void test();
    //static concrete method
    static void info(){
        System.out.println("Info Method");
    }
    //constructor
    Demo(){
        System.out.println("Constructor");
    }
    //static & non-static block'
    static {
        System.out.println("static block");
    }
    {
        System.out.println("non-static block");
    }
}
