package Abstract;
//multi-level inheritance
public class MainApp3 {
    public static void main(String[] args) {
        InstagramV3 v=new InstagramV3();
        v.makePost();
        v.stories();
        v.reels();
    }
}
