package Abstract;

import java.util.Scanner;
//hierarchical inheritance
public class MainApp4 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Select Type:");
        System.out.println("1.Manager\n2.Watchman");
        int choice=sc1.nextInt();
        Employee e=null;
        if(choice==1){
            //manager
            e=new Manager();//upcasting
        } else if (choice==2) {
            //watchman
            e=new Watchman();//upcasting
        }
        e.getDesignation();
        e.getSalary();
    }
}
