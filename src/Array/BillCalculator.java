package Array;

public class BillCalculator {
    public void calculateBill(double[] amounts){
        double[] gstValues=gstCalculation(amounts);
        double[] totalAmts=new double[amounts.length];
        for (int a=0;a< amounts.length;a++){
            totalAmts[a]=amounts[a]+gstValues[a];
        }
        double totalBillAmt=0.0;
        double totalGstAmt=0.0;
        double totalFinalAmt=0.0;
        for (int a=0;a<amounts.length;a++){
            totalBillAmt+=amounts[a];
            totalGstAmt+=gstValues[a];
            totalFinalAmt+=totalAmts[a];
        }
        System.out.println("BillAmt\tGSTAmt\tTotal");
        System.out.println("========================");
        for (int a=0;a<amounts.length;a++){
            System.out.println(amounts[a]+"\t"+gstValues[a]+"\t"+totalAmts[a]);
        }
        System.out.println("=========================");
        System.out.println(totalBillAmt+"\t"+totalGstAmt+"\t"+totalFinalAmt);
    }
    public double[] gstCalculation(double[] amounts){
        double[] gstAmts=new double[amounts.length];
        for (int a=0;a< amounts.length;a++){
            if(amounts[a]<500){
                gstAmts[a]=amounts[a]*0.05;
            }
            else{
                gstAmts[a]=amounts[a]*0.1;
            }
        }
        return gstAmts;
    }
}
