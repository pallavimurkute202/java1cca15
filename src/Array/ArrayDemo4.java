package Array;

import java.util.Scanner;

public class ArrayDemo4 {
    public static void main(String[] args) {
        Scanner sc1 = new Scanner(System.in);
        System.out.println("Enter total no of subject:");
        int size = sc1.nextInt();
        double[] marks = new double[size];
        System.out.println("Enter subject marks");
        double sum = 0.0;
        for (int a = 0; a < size; a++) {
            marks[a] = sc1.nextInt();
            sum += marks[a];
        }
        System.out.println("total marks:" + sum);
        System.out.println("percent:" + sum/size);
    }
}
