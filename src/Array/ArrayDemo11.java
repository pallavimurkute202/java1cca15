package Array;

import java.util.Scanner;

public class ArrayDemo11 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Enter total no of bills");
        int count=sc1.nextInt();
        double[] amounts=new double[count];
        System.out.println("Enter Bill Amt");
        for (int a=0;a<amounts.length;a++){
            amounts[a]=sc1.nextDouble();
        }
        BillCalculator b1=new BillCalculator();
        b1.calculateBill(amounts);
    }
}
