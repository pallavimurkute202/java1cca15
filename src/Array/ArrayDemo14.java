package Array;

import java.util.Scanner;

public class ArrayDemo14 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Enter total no of floors:");
        int floors=sc1.nextInt();
        System.out.println("Enter total no of flats on each floor:");
        int flats=sc1.nextInt();
        int[][] building=new int[floors][flats];
        System.out.println("Enter "+floors*flats+" flat nos:");
        for (int a=0;a<floors;a++){
            for (int b=0;b<flats;b++){
                building[a][b]=sc1.nextInt();
            }
        }
        System.out.println("-----------------------");
        for (int a=0;a<floors;a++){
            System.out.println("Floor no:"+(a+1));
            System.out.println("=====================");
            for (int b=0;b<flats;b++){
                System.out.println("flat no:"+building[a][b]+"\t");
            }
            System.out.println();
        }
    }
}
