package Array;

public class StoreManager {
    static String[] products={"TV","Projector","Mobile"};
    static double[] cost={15000,25000,30000};
    static int[] stock={25,10,50};
    void calculateBill(int choice,int qty){
        boolean found=false;
        for (int a=0;a< products.length;a++){
            if(choice==a+1 && qty<=stock[a]){
                double total=qty*cost[a];
                stock[a]-=qty;
                System.out.println("Total Bill Amount:"+total);
                found=true;
            }
        }
        if(!found){
            System.out.println("Products not found or else out of stock");
        }
    }
}
