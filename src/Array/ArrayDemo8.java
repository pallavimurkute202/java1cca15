package Array;

import java.util.Scanner;

public class ArrayDemo8 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        boolean status=true;
        while(status){
            System.out.println("Select Products:");
            System.out.println("1.TV \n2.Projector \n3.Mobile \n4.Exit");
            int choice= sc1.nextInt();
            StoreManager s1=new StoreManager();
            if(choice==0 || choice==1 || choice==2){
                System.out.println("Enter Quantity:");
                int qty=sc1.nextInt();
                s1.calculateBill(choice,qty);
            }
            else{
                System.out.println("Invalid choice");
                status=false;
            }
        }
    }
}
