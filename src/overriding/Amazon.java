package overriding;

public class Amazon extends Ecommerce{
    @Override
    void sellProduct(int qty, double price) {
        //15% discount
        double total=qty*price;
        double finalAmt=total+total*0.15;
        System.out.println("final amount:"+finalAmt);
    }
}
