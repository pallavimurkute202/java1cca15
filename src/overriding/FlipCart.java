package overriding;
//sub class
public class FlipCart extends Ecommerce{
    @Override
    void sellProduct(int qty, double price) {
        //10% discount
        double total=qty*price;
        double finalAmt=total+total*0.1;
        System.out.println("final amount:"+finalAmt);
    }
}
