package overriding;

import java.util.Scanner;

public class MainApp2 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Enter Quantity:");
        int qty=sc1.nextInt();
        System.out.println("Enter Price");
        double price=sc1.nextDouble();
        System.out.println("Select Ecommerce Platform");
        System.out.println("1.flipcart\n2.Amazon");
        int ch=sc1.nextInt();
        if(ch==1){
            FlipCart f1=new FlipCart();
            f1.sellProduct(qty,price);
        }
        else if(ch==2){
            Amazon a1=new Amazon();
            a1.sellProduct(qty,price);
        }
        else{
            System.out.println("Invalid choice:");
        }
    }
}
