package Inheritance;
//sub class
public class Mobile extends Product{
    double productPrice=35000;
    int qty=10;
    void info(){
        System.out.println(productPrice);
        System.out.println(qty);
        System.out.println(super.productPrice);
        System.out.println(super.qty);
    }
}
