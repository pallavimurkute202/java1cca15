package Inheritance;
//subclass
public class Department extends College{
    public Department(String universityName,String collegeName,String deptName){
        super(universityName,collegeName);
        System.out.println("Department "+deptName);
    }
}
