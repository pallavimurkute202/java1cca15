package Inheritance;
// subclass
public class College extends University{
    //param constructor
    public College(String universityName,String collegeName){
        super(universityName);
        System.out.println("College is "+collegeName);
    }
}
