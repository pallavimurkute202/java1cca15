package Inheritance;

public class MainApp3 {
    public static void main(String[] args) {
        PermanentEmp p1=new PermanentEmp();
        p1.getInfo(101,35000);
        p1.getDesignation("Analyst");
        System.out.println("===================");
        ContractEmp c1=new ContractEmp();
        c1.getInfo(201,45000);
        c1.getContractDetails(24);
    }
}
