package Inheritance;
//super class
public class Master {
    Master(int a){
        System.out.println(a);
    }
    Master(String s){
        this(25);
        System.out.println(s);
    }
    Master(char c){
        this("JAVA");
        System.out.println(c);
    }
}
