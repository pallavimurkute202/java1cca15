package Composition;
//dependency entity
public class Laptop {
    //composition
    static HDD h1=new HDD();
    void getInfo(){
        System.out.println("Company name is HP");
        System.out.println("Price is 45000");
    }
}
