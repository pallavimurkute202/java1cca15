package Interface;
//super interface
@FunctionalInterface
public interface BackEnd {
    void developServerProgram(String language);
}
