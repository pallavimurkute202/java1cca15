package Interface;
//multiple inheritance
public class Software extends FrontEnd implements BackEnd,Database {
    @Override
    public void developServerProgram(String language) {
        System.out.println("Developing Server Program using "+language);
    }

    @Override
    public void designDatabase(String dbVendor) {
        System.out.println("designing database using "+dbVendor);
    }
}
