package Interface;
//super interface
@FunctionalInterface
public interface Database {
    void designDatabase(String dbVendor);
}
