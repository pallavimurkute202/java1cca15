package Interface;
@FunctionalInterface
public interface Wallet {
    void makeBillPayment(double amt);
}
