package LoopingStatements;

import java.util.Scanner;

public class SwitchDemo {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Select language");
        System.out.println(" 1.Java \n 2.Python \n 3.PHP");
        int ch= sc1.nextInt();;
        switch (ch){
            case 1:
                System.out.println("selected Java");
                break;
            case 2:
                System.out.println("selected python");
                break;
            case 3:
                System.out.println("selected PHP");
                break;
            default:
                System.out.println("Invalid Choice");
        }
    }
}
