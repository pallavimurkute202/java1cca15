package LoopingStatements;

import java.util.Scanner;

public class ForLoopDemo5 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Enter start point:");
        int start=sc1.nextInt();
        System.out.println("Enter end point");
        int end=sc1.nextInt();
        double sum=0.0;
        for (int i=start;i<=end;i++){
            if(i%2==0){
                sum+=i;
            }
        }
        System.out.println("Sum of all even numbers:"+sum);
    }
}
