package LoopingStatements;

import java.util.Scanner;

public class ForLoopDemo3 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Enter start point:");
        int s=sc1.nextInt();
        System.out.println("Enter end point");
        int e=sc1.nextInt();
        for (int i=s;i<=e;i++) {
            if (i % 2 == 0) {
                System.out.println(i*i);
            }
        }
    }
}


