package LoopingStatements;

import java.util.Scanner;

public class ForLoopDemo6 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Enter total no of users:");
        int count=sc1.nextInt();
        for (int i=1;i<=count;i++){
            System.out.println("Enter Username:");
            String name=sc1.next();
            System.out.println("Welcome "+name);
        }
    }
}
