package LoopingStatements;

import java.util.Scanner;

public class WhileLoopDemo3 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Enter no1:");
        int no1=sc1.nextInt();
        System.out.println("Enter no2:");
        int no2=sc1.nextInt();
        System.out.println("1.Addition \t 2.Substraction \t 3.Exit");
        int ch=sc1.nextInt();
        boolean status=true;
        while(status){
            if (ch==1) {
                System.out.println(no1+no2);
            }
            else if (ch==2){
                System.out.println(no1-no2);
            }
            else {
                status=false;
            }
        }
    }
}
