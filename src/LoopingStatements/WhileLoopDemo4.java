package LoopingStatements;

public class WhileLoopDemo4 {
    public static void main(String[] args) {
        int a=4;
        while(a>4){
            a--;
        }
        System.out.println(a);
        int b=4;
        do{
            b--;
        }while(b>4);
        System.out.println(b);
    }
}
