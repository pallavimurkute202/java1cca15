package overloading;

public class MainApp4 {
    public static void main(String[] args) {
        System.out.println("Main Method 1");
        main(25);
        main('J');
    }

    public static void main(int a) {
        System.out.println("Main Method 2");
        System.out.println("A:"+a);
    }

    public static void main(char c) {
        System.out.println("Main Method 3");
        System.out.println("C:"+c);
    }
}
