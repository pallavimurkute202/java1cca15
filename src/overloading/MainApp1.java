package overloading;

import java.util.Scanner;

public class MainApp1 {
    public static void main(String[] args) {
        Student s1=new Student();
        Scanner sc1=new Scanner(System.in);
        System.out.println("Select search criteria");
        System.out.println("1.Search by name");
        System.out.println("2.Search by contact");
        int choice=sc1.nextInt();
        if(choice==1){
            System.out.println("Enter Name:");
            String name=sc1.next();
            s1.search(name);
        } else if (choice==2) {
            System.out.println("Enter contact no:");
            int contact=sc1.nextInt();
            s1.search(contact);
        }
        else {
            System.out.println("Invalid Choice:");
        }
    }
}
