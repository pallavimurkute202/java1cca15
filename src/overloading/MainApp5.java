package overloading;

import java.util.Scanner;

public class MainApp5 {
    public static void main(String[] args) {
        Ecommerce e1=new Ecommerce();
        Scanner sc1=new Scanner(System.in);
        System.out.println("Enter amount paid:");
        int amt=sc1.nextInt();
        System.out.println("Select payment option");
        System.out.println("1.Card \n 2.UPI \n 3.Net Banking");
        int ch=sc1.nextInt();
        if(ch==1) {
            System.out.println("Enter card no:");
            int cno=sc1.nextInt();
            System.out.println("Enter cvv no:");
            int cvv=sc1.nextInt();
            System.out.println("Enter Expiry date");
            double exDate=sc1.nextDouble();
            e1.makePayment(cno,cvv,exDate,amt);
        } else if (ch==2) {
            System.out.println("Enter pin no:");
            int pinNo=sc1.nextInt();
            e1.makePayment(pinNo,amt);
        } else if (ch==3) {
            System.out.println("Enter username:");
            String user=sc1.next();
            System.out.println("Enter password:");
            String pwd=sc1.next();
            e1.makePayment(user,pwd,amt);
        }else{
            System.out.println("Invalid Choice");
        }
    }
}
