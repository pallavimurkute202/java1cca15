package typeCasting;

public class CastingDemo3 {
    public static void main(String[] args) {
        char ch='J';
        char ch1='P';
        int x1=ch;
        int x2=ch1;
        System.out.println(x1+"\t"+x2);
        int x3=97;
        char ch2= (char) x3;
        System.out.println(ch2);
        double d=65.0;
        char ch3= (char) d;
        System.out.println(ch3);
    }
}
