package typeCasting;

import java.util.Scanner;

public class UpCasting3 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Enter Qty");
        int qty=sc1.nextInt();
        System.out.println("Enter price");
        double price=sc1.nextDouble();
        System.out.println("Select Machine");
        System.out.println("1.Laptop\n2.Projector");
        int ch=sc1.nextInt();
        Machine m1=null;
        if(ch==1){
            m1=new Laptop();
        } else if (ch==2) {
            m1=new Projector();
        }
        else{
            System.out.println("Invalid choice");
        }
        m1.getType();
        m1.calculateBill(qty,price);
    }
}
