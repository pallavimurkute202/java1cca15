package typeCasting;

public class CastingDemo4 {
    public static void main(String[] args) {
        short s1= (short) 1325698;
        System.out.println(s1);
        long l1=896754321897565l;
        int x1= (int) l1;
        System.out.println(x1);
    }
}
