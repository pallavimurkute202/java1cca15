package typeCasting;

import java.util.Scanner;

public class UpCasting4 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Select service provider:");
        System.out.println("1.AirAsia\n2.Indigo");
        int ch=sc1.nextInt();
        System.out.println("Select route:");
        System.out.println("0.PUNE-DELHI\n1.MUMBAI-CHENNAI\n2.KOLKATA-BANGALORE");
        int routeChoice= sc1.nextInt();
        System.out.println("Enter no of tickets:");
        int ticket= sc1.nextInt();
        Goibibo g1=null;
        if(ch==0){
            g1=new AirAsia();
        } else if (ch==1) {
            g1=new Indigo();
        }
        g1.bookTicket(ticket,routeChoice);
    }
}
