package typeCasting;

public class Laptop extends Machine{
    @Override
    void getType() {
        System.out.println("machine is laptop");
    }

    @Override
    void calculateBill(int qty, double price) {
        double total=qty*price;
        double finalAmt=total+total*0.1;
        System.out.println("Final Amt:"+finalAmt);
    }
}
