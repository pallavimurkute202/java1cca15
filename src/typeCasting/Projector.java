package typeCasting;

public class Projector extends Machine{
    void getType(){
        System.out.println("Machine type is projector");
    }

    @Override
    void calculateBill(int qty, double price) {
        double total=qty*price;
        double finalAmt=total+total*0.1;
        System.out.println("Final Amt:"+finalAmt);
    }
}
