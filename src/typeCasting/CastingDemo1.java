package typeCasting;

public class CastingDemo1 {
    public static void main(String[] args) {
        int a=15;
        double b=23.35;
        System.out.println(a+"\t"+b);
        int c= (int) 35.85;//narrowing
        double d=26;//widening
        System.out.println(c+"\t"+d);
    }
}
