package typeCasting;

public class Indigo extends Goibibo{
    double[] cost={3000.25,5000.25,8000.25};

    @Override
    void bookTicket(int qty, int routeChoice) {
        boolean found=false;
        for (int a=0;a<routes.length;a++){
            if(routeChoice==a){
                double total=qty*cost[a];
                System.out.println("total amount:"+total);
                found=true;
            }
        }
        if(!found)
            System.out.println("route not found");
    }
}
