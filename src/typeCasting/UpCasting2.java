package typeCasting;

public class UpCasting2 {
    public static void main(String[] args) {
        LED l1=new LED();
        l1.displayPicture();
        l1.streamingQuality();
        LCD l2=new LCD();
        l2.displayPicture();
        l2.streamingQuality();
        TV t1;
        t1=new LCD();//upcasting
        t1=new LED();//upcasting
        t1.displayPicture();
    }
}
