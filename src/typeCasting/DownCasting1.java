package typeCasting;

public class DownCasting1 {
    public static void main(String[] args) {
        Master m1=new Central();//upcasting
        m1.test();
        Central c1= (Central) new Master();
        c1.test();
        c1.display();
    }
}
