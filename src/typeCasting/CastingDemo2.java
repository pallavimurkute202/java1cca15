package typeCasting;

public class CastingDemo2 {
    public static void main(String[] args) {
        int x=53;
        float y=35.45f;
        int a= (int) y;//narrowing
        float b=x;//widening
        System.out.println(a+"\t"+b);
    }
}
