package typeCasting;

public class DownCasting3 {
    public static void main(String[] args) {
        Bike b1=new ElectricBike();//upcasting
        b1.getType();
        ElectricBike e1=(ElectricBike) b1;//downcasting
        e1.getType();
        e1.batterySize();
    }
}
