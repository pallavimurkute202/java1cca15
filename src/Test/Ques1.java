package Test;

import java.util.Scanner;

//Write a java program to accept an element from the user and find out the position of that element inside the given array
//Declare an array of type integer with some predifined values
public class Ques1 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Enter value of element:");
        int a=sc1.nextInt();
        int[] arr1={45,67,89,98,78,90};
        boolean found =false;
        for(int i=0;i<arr1.length;i++) {
            if (arr1[i] == a) {
                System.out.println("number found in given array");
                found = true;
                System.out.println("Position of number:" + i);
                break;
            }
        }
        if((!found==true))
            System.out.println("Number not found");
        }
    }
