package Test;

public class Ques2 {
    public static void main(String[] args) {
        int[] arr1 = {10, 20, 30};
        int[] arr2 = {40, 50, 60};
        int l1=arr1.length;
        int l2=arr2.length;
        int[] result=new int[l1+l2];
        for (int i=0;i<arr1.length;i++){
            result[i]=arr1[i];
            result[l1+i]=arr2[i];
        }
        for (int i=0;i<result.length;i++){
            System.out.print(result[i]+" ");
        }
    }
}
